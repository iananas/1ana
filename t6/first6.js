function f(x)
{
    return x**3 - 6*x**2 + 11*x-6
};
var ksi, a, b, eps;
function dihotomia( a, b, eps )
{
    ksi = ( a + b ) / 2.0;
    if (( Math.abs( f(a) - f(b) ) <= eps ) || ( Math.abs( f(ksi) ) <= eps ))
    {
        return ( a + b ) / 2.0;
    }
    
    if ( f( a )*f( ksi ) <= 0.0 )
    {
        return dihotomia( a, ksi, eps );
    }
    
    else
    {
        return dihotomia( ksi, b, eps );
    }
    
}
document.writeln('корни уравнения:'); 
a = -2;
b = -1;
eps = 0.00001;
var firstK = dihotomia( a, b, eps );
document.writeln('<br>',firstK);
a = 0;
b = 1;
var secondK = dihotomia( a, b, eps );
document.writeln('<br>',secondK);
a = 1;
b = 2;
var thirdK = dihotomia( a, b, eps );
document.writeln('<br>',thirdK);
var sum = Math.pow(firstK,3) + Math.pow(secondK,3) + Math.pow(thirdK,3);
document.writeln('<br>сумма кубов уравнения:<br>',sum);


